function displayResult(res, size) 

% Data
x = res(:,1);
y2 = res(:,2);
resSize = length(x)

% Original plot
bar(x, y2, 0.5, 'FaceColor', [0, 0.5, 1]);
labels = x;
set(gca, 'XTickLabel', labels);  
title('Row Info',"fontsize", 14);
xlabel('Values');
ylabel('Apearence');

% Additional plot
y = max(y2) / 2;              % "located on the high Y=50%"
p = y2*100/size
values = [num2str(p,"%5.1f") repelem(' %', resSize, 1)];
text( x - 0.2, ...            % x values to put text (some negative displacement for "centrally centered with each bar column")
      repelem(y, resSize, 1), ...   % y values to put text (four times y)
      values, ...             % Actual values as text plus "%" to put
      'Color', 'r', ...       % Red text
      'FontWeight', 'bold');  % Bold text



