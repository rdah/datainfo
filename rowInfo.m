function [res] = rowInfo(data) 

nbrLine = length(data)
res = [data(1), 1]

for i=2:nbrLine,
	r = find(res(:,1)==data(i))
	if length(r) == 0,
		res = [res; [data(i), 1]]
	else	 
		res(r,2) = res(r,2) + 1
	end;
	
end;

